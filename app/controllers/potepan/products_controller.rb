class Potepan::ProductsController < ApplicationController
  MAX_RELATED_PRODUCTS = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.includes(master: [:default_price, :images]).sample(MAX_RELATED_PRODUCTS)
  end
end
