module ApplicationHelper
  BASE_TITLE = "BIGBAG Store".freeze

  def full_title(page_title)
    return BASE_TITLE if page_title.blank?
    "#{page_title} - #{BASE_TITLE}"
  end
end
