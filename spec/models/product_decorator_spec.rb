require 'rails_helper'

describe Potepan::ProductDecorator, type: :model do
  let(:taxon_a)        { create(:taxon) }
  let(:taxon_b)        { create(:taxon) }
  let(:other_taxon)    { create(:taxon) }
  let(:product)        { create(:product, taxons: [taxon_a, taxon_b]) }
  let(:other_products) { create_list(:product, 2, name: "Others", taxons: [other_taxon]) }
  let(:products) do
    create_list(:product, 2, name: "Related", taxons: [taxon_a, taxon_b])
    create_list(:product, 3, name: "Related", taxons: [taxon_b])
  end

  describe "related_products" do
    it "includes products" do
      expect(product.related_products).to include(*products)
    end

    it "does not include other_products" do
      expect(product.related_products).not_to include(*other_products)
    end

    it "does not have duplicate products" do
      related_products = product.related_products
      expect(related_products).to eq related_products.uniq
    end

    it "product is not included in related products" do
      expect(product.related_products).not_to include product
    end
  end

  describe "image_pickup" do
    let(:first_image)  { create(:image) }
    let(:second_image) { create(:image) }

    context "when product has no images" do
      it "returns Spree::Image.new" do
        expect(product.images.count).to eq 0
        expect(product.image_pickup.url(:product)).to eq "noimage/product.png"
      end
    end

    context "when product has images" do
      it "returns a product image" do
        product.images << first_image << second_image
        expect(product.image_pickup).to eq first_image
      end
    end
  end
end
