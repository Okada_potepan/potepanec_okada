require 'spec_helper'

RSpec.describe ApplicationHelper, :type => :helper do
  describe "title" do
    it "page_titleが空文字" do
      expect(full_title("")).to eq "BIGBAG Store"
    end
    it "page_titleが空でない" do
      expect(full_title("BASE_TITLE")).to eq "BASE_TITLE - BIGBAG Store"
    end
  end
end
