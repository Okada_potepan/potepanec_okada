require 'rails_helper'

RSpec.feature "Products", type: :feature do
  include ApplicationHelper
  subject { page }

  let(:taxon_a)        { create(:taxon) }
  let(:taxon_b)        { create(:taxon) }
  let(:other_taxon)    { create(:taxon) }
  let(:product)        { create(:product, taxons: [taxon_a, taxon_b]) }
  let(:other_products) { create_list(:product, 2, name: "Others", taxons: [other_taxon]) }
  let!(:products) do
    create_list(:product, 2, name: "Related", taxons: [taxon_a, taxon_b])
    create_list(:product, 3, name: "Related", taxons: [taxon_b])
  end

  before do
    visit potepan_product_path(product.id)
  end

  describe "check main layout" do
    it { is_expected.to have_title full_title(product.name) }

    it "has a correct pageHeader" do
      within ".pageHeader" do
        within(".page-title") { is_expected.to have_content product.name }
        within(".active") { is_expected.to have_content product.name }
      end
    end

    it "has correct details for the product in media-body" do
      within ".media-body" do
        is_expected.to have_content product.name
        is_expected.to have_content product.display_price
        is_expected.to have_content product.description
      end
    end

    it "has the link that links to potepan_category_path" do
      is_expected.to have_link '一覧ページへ戻る', href: potepan_category_path(taxon_a.id)
    end
  end

  describe "check related_products layout" do
    it "has 4 related products infomation" do
      within ".productsContent" do
        is_expected.to have_selector("img", count: 4)
        is_expected.to have_link("Related", count: 4)
      end
    end

    it "does not have other_products information" do
      within ".productsContent" do
        is_expected.to have_no_link("Others")
      end
    end

    it "has links to potepan_product_path" do
      within(".page-title") do
        is_expected.to have_content(product.name)
      end

      first(".productBox").click_on "Related-img"

      within(".page-title") do
        is_expected.to have_content("Related")
      end
    end
  end
end
