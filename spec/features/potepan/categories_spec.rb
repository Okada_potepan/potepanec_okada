require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let!(:other_taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let!(:taxonomy) { create(:taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:other_product) { create(:product, taxons: [taxon]) }
  let!(:test_product) { create(:product, name: "test bag", taxons: [other_taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it "カテゴリと紐づいた商品名、価格名を表示する" do
    expect(page).to have_link taxon.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  it "別の商品詳細ページへのリンク" do
    expect(page).to have_link other_taxon.name
    expect(page).to have_content other_product.name
    expect(page).to have_content other_product.display_price
  end

  it "別の商品名を表示しない" do
    expect(page).not_to have_content "test bag"
  end

  it "categoriesページがproduct pageのリンク" do
    expect(page).to have_link product.name
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
