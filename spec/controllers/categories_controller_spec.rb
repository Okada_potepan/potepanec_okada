require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let(:bag_taxon) { create(:taxon, name: 'bag') }
  let(:products) { create_list(:product, 5) }

  describe "GET categories show with bag_taxon" do
    before do
      get :show, params: { id: bag_taxon.id }
    end

    it "responds successfully" do
      expect(response).to be_successful
    end

    it "@taxon have bag_taxon" do
      expect(assigns(:taxon)).to eq bag_taxon
    end

    it "@products have bag_taxon.products" do
      products.each do |product|
        product.taxons << bag_taxon
      end
      expect(assigns(:products)).to match_array(products)
    end
  end
end
