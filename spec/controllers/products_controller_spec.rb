require 'rails_helper'
require 'spree/testing_support/factories'

describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let(:taxon_a)        { create(:taxon) }
    let(:taxon_b)        { create(:taxon) }
    let(:other_taxon)    { create(:taxon) }
    let(:product)        { create(:product, taxons: [taxon_a, taxon_b]) }
    let(:other_products) { create_list(:product, 2, name: "Others", taxons: [other_taxon]) }
    let!(:products) do
      create_list(:product, 2, name: "Related", taxons: [taxon_a, taxon_b])
      create_list(:product, 3, name: "Related", taxons: [taxon_b])
    end

    before do
      get :show, params: { id: product.id }
    end

    it "responds successfully" do
      expect(response).to be_successful
    end

    it 'assigns @product' do
      expect(assigns(:product)).to eq product
    end

    it 'render the :show template' do
      expect(response).to render_template :show
    end

    it "has 4 products" do
      expect(assigns(:related_products).count).to eq 4
    end

    it "has only related_products" do
      expect(product.related_products).to include(*assigns(:related_products))
    end
  end
end
